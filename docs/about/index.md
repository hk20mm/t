# About 

## Lorain County Community College

![](../images/lccc-campus.jpg)

Founded in 1963, Lorain County Community College has been helping local people get the training and education they need to live their best life for more than 50 years.
 
LCCC is proud to serve as a cornerstone for Lorain County. The College provides the opportunity for students to complete:
- Bachelor’s degree
- Associate degrees
- Short-term certificates
- Workforce training

Through LCCC’s University Partnership, you can earn a bachelor’s or master’s degree from 14 Ohio universities and colleges. We are also the first community college in Ohio to offer its own bachelor’s degree. 
LCCC keeps tuition low – currently second lowest tuition in Ohio – so you can graduate with the skills you need for employment – and not a lot of debt.
As a result of our ongoing commitment to our students, LCCC was named the top community college in the country for Excellence in Student Success by the American Association of Community Colleges (AACC).

## About the Lorain County Community Fab Lab

![](../images/campana.jpg)

The LCCC Fab Lab is located in the new Campana Center for Ideation and Invention.

For more information visit the Fab Lab website at <u>[https://www.lorainccc.edu/campana/fab-lab/](https://www.lorainccc.edu/campana/fab-lab/)</u>


