# Home

![](./images/lcccfablogo2.png)

##  Fab Academy 2019 Students
<u>

- [Brigette O'Neill](http://fabacademy.org/2019/labs/lccc/students/brigette-oneill)
- [Mess Wright](http://fabacademy.org/2019/labs/lccc/students/mess-wright)
- [Jim Hart](http://fabacademy.org/2019/labs/lccc/students/jim-hart)
- [Brent Richardson](http://fabacademy.org/2019/labs/lccc/students/brent-richardson)
- [Joseph Johnnie](http://fabacademy.org/2019/labs/lccc/students/joseph-johnnie)

</u>
## Group Assignments
<u>

- [Mechanical Design](http://fabacademy.org/2019/labs/lccc/students/group-assignments)
- [Machine Design](http://fabacademy.org/2019/labs/lccc/students/group-assignments)


</u>


## Fab Academy 2019 Instructors

- R. Scott Zitek
- Chris Rohal